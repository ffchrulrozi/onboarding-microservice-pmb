﻿using Microsoft.EntityFrameworkCore;
using Pmb.Model;

namespace Pmb
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options) {}
        public DbSet<Registrant> Registrants { set; get; }
    }
}
