﻿namespace Pmb.Model
{
    public class Registrant
    {
        public int ID { set; get; }
        public string Name { set; get; }
        public string Major { set; get; }
        public string Address { set; get; }
    }
}
