﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pmb.Model;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Pmb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistrantController : ControllerBase
    {
        ApplicationDbContext db;

        public RegistrantController(ApplicationDbContext db)
        {
            this.db = db;
        }
        // GET: api/<RegistrantController>
        [HttpGet]
        public object Get()
        {
            return db.Registrants.ToList();
        }

        // POST api/<RegistrantController>
        [HttpPost]
        public void Post([FromBody] Registrant registrant)
        {
            db.Registrants.Add(registrant);
            db.SaveChanges();
        }
        [HttpPut]
        public object Put(int id, Registrant registrant)
        {
            if (ModelState.IsValid)
            {
                db.Entry(registrant).State = EntityState.Modified;
                db.SaveChanges();
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete]
        public object Delete(int id)
        {
            Registrant registrant = db.Registrants.Find(id);
            if (registrant == null)
            {
                return NotFound();
            }

            db.Registrants.Remove(registrant);
            db.SaveChanges();

            return Ok(registrant);
        }
    }
}
